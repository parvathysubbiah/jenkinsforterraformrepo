resource "aws_instance" "testp" {
  ami           = "ami-0742b4e673072066f"
  instance_type = "t2.micro"
  key_name = "ansiblekeypair"
  tags = {
   Name = "EC2_Amazonlinux"
  }
  security_groups = ["testsg"]

  provisioner "local-exec" {
    command = "echo ${self.public_ip} >> hostinventory;ansible-playbook -i hostinventory helloworld/tasks/main.yml --private-key=/backup/ansilekeypair.pem -u ec2-user"
}


}
